# Thinkpad Battery Threshold Extension

**Gnome extension for Enable/Disable battery threshold on Lenovo ThinkPad laptops**

If you mainly use the system with the AC power adapter connected and only use the battery sporadically, you can increase battery life by setting the maximum charge value to less than 100%. This is useful because batteries that are used sporadically have a longer lifespan when kept at less than full charge.

Compatible with Lenovo ThinkPad series from model year 2011 (theoretically).

To check if the function is available, see if the files **charge_control_start_threshold** and **charge_control_end_threshold** (valid alternatives: **charge_start_threshold** and **charge_stop_threshold**) exist in the **/sys/class/power_supply/BAT[0-1]** directory, also you must have permissions to read and write (in case you do not have write permissions, the root password will be requested to modify the values).

## Install
1. Open [Thinkpad Battery Threshold] on GNOME Shell Extensions site.
2. Click slider to install extension.
3. [Skip password prompt](#skip-password-prompt) (Optional)

[Thinkpad Battery Threshold]: https://extensions.gnome.org/extension/4798/thinkpad-battery-threshold/

## Skip password prompt
To modify the battery charge thresholds it is necessary to have administrative permissions, so the extension requests the password when applying any modification.
If you want to not be prompted for the password, you can follow the instructions below:

**Disclaimer: These methods set the permissions necessary for any user to modify the battery threshold values.**

### **Udev mode**
Install the **[Udev rule](/others/99-thinkpad-thresholds-udev.rules)** provided in the path `/etc/udev/rules.d/`.
```sh
sudo wget https://gitlab.com/marcosdalvarez/thinkpad-battery-threshold-extension/-/raw/main/others/99-thinkpad-thresholds-udev.rules -N -P /etc/udev/rules.d/
```
Remember to remove the rules if you decide to uninstall the extension.
```sh
sudo rm -f /etc/udev/rules.d/99-thinkpad-thresholds-udev.rules
```

### **Alternative Polkit mode**
First, check your polkit version using the command:
```sh
pkaction --version
```

### Polkit version >= 0.106
Install the **[Polkit rule](/others/99-thinkpad-thresholds-polkit.rules)** provided in the path `/etc/polkit-1/rules.d/`.
```sh
sudo wget https://gitlab.com/marcosdalvarez/thinkpad-battery-threshold-extension/-/raw/main/others/99-thinkpad-thresholds-polkit.rules -N -P /etc/polkit-1/rules.d/
```
Remember to remove the rules if you decide to uninstall the extension.
```sh
sudo rm -f /etc/polkit-1/rules.d/99-thinkpad-thresholds-polkit.rules
```

### Polkit version < 0.106
Not yet implemented.

## Translations
You can help us translating extension to your language.

[Translate on Weblate](https://hosted.weblate.org/engage/thinkpad-battery-threshold/)

[![Estado de la traducción](https://hosted.weblate.org/widgets/thinkpad-battery-threshold/-/horizontal-auto.svg)](https://hosted.weblate.org/engage/thinkpad-battery-threshold/)

## Screenshots
![Gnome 43](/images/Gnome%2043.png)*Gnome 43*

![Gnome 42](/images/Gnome%2042.png)*Gnome 42*

![Gnome 41](/images/Gnome%2041.png)*Gnome 41*
